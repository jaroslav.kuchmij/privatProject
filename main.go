//Автор Кучмий Ярослав
package main

import (
	"fmt"
	"os"
	"io/ioutil"
	"strings"
	"regexp"
)

var nameFile = "test.txt"

const message1 = "\tCreate file - Done!\n"

var choiseInt int

var choiseString string

func main() {
	mainMenu()
	choiseMenu()

}

//Выбор пользователя
func choiseMenu() {
	maxValue := 9
	minValue := 1

	for{
		fmt.Print("Choice:")
		fmt.Scanln(&choiseInt)
		if choiseInt <= maxValue && choiseInt >= minValue {
			switch choiseInt {
			case 1:
				createFile()
			case 2:
				deleteFile()
			case 3:
				getAllPhonebook()
			case 4:
				addNewContact()
			case 5:
				searchContactByNamber()
			case 6:
				updateContactByNamber()
			case 7:
				deleteContactByNamber()
			case 8:
				mainMenu()
			case 9:
				return
			} 
		} else {
		fmt.Println("Incorrect value")
		return
		}
	}
}

//Функция создания файла
func createFile() {
	if checkIsNotExist() {
		file, err := os.Create(nameFile)
		if err != nil {
			return
		}
		defer file.Close()
		fmt.Println(message1)
		return
	}

	fmt.Println("The file already exists!")
}

//Функция удаления файла
func deleteFile() {
	if !checkIsNotExist() {
		os.Remove(nameFile)
		fmt.Println("file deleted!")
		return
	}
	fmt.Println("The file does not exist!")
}

//Функция чтения файла
func getAllPhonebook() {
	if !checkIsNotExist() {
		file, err := os.Open(nameFile)
		if err != nil {
			return
		}
		defer file.Close()

		stat, err := file.Stat()
		if err != nil {
			return
		}

		bs := make([]byte, stat.Size())
		_, err = file.Read(bs)
		if err != nil {
			return
		}

		str := string(bs)
		fmt.Println(str)
		return
	}
	fmt.Println("The file does not exist!")
}

//Внесение в справочник номера и ФИО контакта
func addNewContact() {
	fmt.Println("Enter the phone number (format: +380981111111):")
	fmt.Scanln(&choiseString)

	if !checkIsNotExist() {
		if !checkValidNamber(choiseString) {
			fmt.Println("Invalid namber")
			return
		}

		if bool, value := searchNamberInFile(choiseString); bool == true {
			fmt.Println("This number exists")
			fmt.Println(value)
			return
		}
	}
	setNumber(choiseString)
	choiseString = ""

	fmt.Println("Enter Name:")
	fmt.Scanln(&choiseString)
	setName(choiseString)

	fmt.Println("Contact added")
}

//Запись номера в справочник
func setNumber(incomingValue string) {
		f, err := os.OpenFile(nameFile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0600)
		if err != nil {
			panic(err)
		}
		defer f.Close()

		if _, err = f.WriteString(string(incomingValue)+"|"); err != nil {
			panic(err)
		}
		return
}

//Запись ФИО в справочник
func setName(incomingValue string) {
		f, err := os.OpenFile(nameFile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0600)
		if err != nil {
			panic(err)
		}
		defer f.Close()

		if _, err = f.WriteString(incomingValue+"\n"); err != nil {
			panic(err)
		}
		return
}

//Поиск контакта в телефонной книге
func searchContactByNamber() {

 	fmt.Println("\t\tSearching")
 	//writeFile()
 	fmt.Print("Enter the phone number (format: +380981111111):")
	fmt.Scanln(&choiseString)

	if !checkValidNamber(choiseString) {
		fmt.Println("Invalid namber")
		return
	}

	if bool, value := searchNamberInFile(choiseString); bool == true {
		fmt.Println(value)
		return
	}
	fmt.Println("This number does not exist")
 } 

//Поиск совпадений по номеру в файле
func searchNamberInFile(incomingValue string) (bool, string) {
	temp := splitFile()    
    for _, item0 := range temp  {
    	temp1 := strings.Split(item0, "|")
    	for line, item := range temp1 {
    		if line == 0 {
    			if checkSerch(incomingValue, item) {
    				return true, item0
    			}
    		}
    	}
    	
    }
	return false, ""
}

//Изменение Имени по номеру телефона
func updateContactByNamber() {
	fmt.Println("\t\tUpdating phone namber")
 	fmt.Print("Enter the phone number (format: +380981111111):")
	fmt.Scanln(&choiseString)
	if !checkValidNamber(choiseString) {
		fmt.Println("Invalid namber")
		return
	}
	temp := splitFile()
	for line0, item0 := range temp  {
    	temp1 := strings.Split(item0, "|")
    	for line, item := range temp1 {
    		if line == 0 {
    			if checkSerch(choiseString, item) {
    				fmt.Println("Enter the new Name")
    				fmt.Scanln(&choiseString)
    				temp1[1] = choiseString
    				output := strings.Join(temp1, "|")
    				temp[line0] = output
    				output1 := strings.Join(temp, "\n")
    				err := ioutil.WriteFile(nameFile, []byte(output1), 0644)
				        if err != nil {
				                panic(err)
				        }
				    fmt.Println("Updating phone namber - OK")
				    return
    			}
    		}
    	}
    	
    }
    fmt.Println("This number does not exist")
    return	
}

//Удаление Строки по номеру
func deleteContactByNamber() {
	fmt.Println("\t\tDeleting phone namber")
 	fmt.Print("Enter the phone number (format: +380981111111):")
	fmt.Scanln(&choiseString)
	if !checkValidNamber(choiseString) {
		fmt.Println("Invalid namber")
		return
	}
	temp := splitFile()
	for line0, item0 := range temp  {
    	temp1 := strings.Split(item0, "|")
    	for line, item := range temp1 {
    		if line == 0 {
    			if checkSerch(choiseString, item) {
				    copy(temp[line0:], temp[line0+1:])
				    temp[len(temp)-1] = "" // обнуляем "хвост"
             		temp = temp[:len(temp)-1]
            		output1 := strings.Join(temp, "\n")
    				err := ioutil.WriteFile(nameFile, []byte(output1), 0644)
				        if err != nil {
				                panic(err)
				        }
				    fmt.Println("Deleting phone namber - OK")
				    return
    			}
    		}
    	}
    	
    }
    fmt.Println("This number does not exist")


}

//Проверка входящего номера на корректность
func checkValidNamber(incomingValue string) bool {
	var validID = regexp.MustCompile(`^[+][3][8][0][0-9\\s\\(\\)\\+]{9}$`)
	res := validID.MatchString(incomingValue)
	return res
}

//Проверка на существование файла
func checkIsNotExist() bool {
	var _, err = os.Stat(nameFile)
	return os.IsNotExist(err)
}

//Проверка входящего значения на совпадение с проверочным значением при поиске
func checkSerch(incomingValue string, checkValue string) bool {
	res := strings.Contains(checkValue, incomingValue)
	return res
}

//Разбивка файла на строки
func splitFile() []string {
	/* ioutil.ReadFile returns []byte, error */
    data, err := ioutil.ReadFile(nameFile)
    if err != nil {
		panic(err)
	}
    /* ... omitted error check..and please add ... */
    /* find index of newline */
    file := string(data)
    /* func Split(s, sep string) []string */
    res := strings.Split(file, "\n")
    return res
}

//Список главного меню 
func mainMenu() {
	fmt.Println("\t\tPhonebook")
	fmt.Println("Choice of action:")
	fmt.Println("\t1 - Create file")
	fmt.Println("\t2 - Delete file")
	fmt.Println("\t3 - View file")
	fmt.Println("\t4 - Add phone namber")
	fmt.Println("\t5 - Search phone namber")
	fmt.Println("\t6 - Update phone namber")
	fmt.Println("\t7 - Delete phone namber")
	fmt.Println("\t8 - Menu")
	fmt.Println("\t9 - Exit")
}
